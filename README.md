# Maze

## Запуск

```
npm i
npm start
```

## Описание

После этого откройте <http://localhost:5000/>, там будет доступна визуализация процесса прохождения лабиринта.

Решение нужно писать в файле src/main.js (HMR не доступен, необходимо обновлять страницу вручную).

У game есть асинхронные функции, которые позволяют двигаться от любой ячейки влево, вправо, вверх или вниз. При попытке шагнуть в стену или из непосещённой клетки методы кидают ошибку. Также асинхронная функция получения состояния ячейки работает только для посещённых ячеек, для остальных – кидает ошибку.

Ось x в лабиринте идет слева-направо, y - сверху-вниз.

## Формат данных

```
export interface Point {
    x: number;
    y: number;
}
```

```
export interface Game {
    // Попытаться шагнуть из клетки лабиринта вверх
    up(x: number, y: number): Promise<void>;
    // Попытаться шагнуть из клетки лабиринта вниз
    down(x: number, y: number): Promise<void>;
    // Попытаться шагнуть из клетки лабиринта влево
    left(x: number, y: number): Promise<void>;
    // Попытаться шагнуть из клетки лабиринта вправо
    right(x: number, y: number): Promise<void>;

    // Получить состояние клетки лабиринта
    state(x: number, y: number): Promise<{
        top: boolean; // можно ли шагать вверх
        bottom: boolean; // можно ли шагать вниз
        left: boolean; // можно ли шагать влево
        right: boolean; // можно ли шагать вправо
        start: boolean; // ячейка - стартовая
        finish: boolean; // ячейка - финиш
    }>;
}
```
