// Не деструктурируйте game, ваше решение не будет проходить тесты.

export default async function main(game, { x, y }) {
  const coords = {
    x,
    y
  }
  const directions = [ 'right', 'down', 'left', 'up' ]
  const reversDirections = {
    up: 'down',
    down: 'up',
    left: 'right',
    right: 'left'
  }
  const directionTransform = (direction, revers = false) => {
    if (revers) {
      switch (direction) {
        case 'top': {
          return 'up'
        }
        case 'bottom': {
          return 'down'
        }

        default: {
          return direction
        }
      }
    }

    switch (direction) {
      case 'up': {
        return 'top'
      }
      case 'down': {
        return 'bottom'
      }

      default: {
        return direction
      }
    }
  }
  const getDirection = state => directions.find(direction => state[directionTransform(direction)] === true)
  const getRandomDirection = () => directions[Math.floor(Math.random() * directions.length)]
  const doCoords = direction => {
    switch (direction) {
      case 'up': {
        coords.y--
        break
      }
      case 'down': {
        coords.y++
        break
      }
      case 'left': {
        coords.x--
        break
      }
      case 'right': {
        coords.x++
        break
      }
    }
  }
  const undoCoords = direction => {
    switch (direction) {
      case 'up': {
        coords.y++
        break
      }
      case 'down': {
        coords.y--
        break
      }
      case 'left': {
        coords.x++
        break
      }
      case 'right': {
        coords.x--
        break
      }
    }
  }

  await game.right(coords.x, coords.y)

  let wallAhead = false
  let count = 0
  const maxCount = 100
  while (await game.state(coords.x, coords.y).finish !== true && count < maxCount) {
    count++

    wallAhead = false

    const state = await game.state(coords.x, coords.y)
    const direction = getDirection(state)

    while (!wallAhead) {
      try {
        console.log('> move', count, direction, coords, state)
        await game[direction](coords.x, coords.y)
        doCoords(direction)
      } catch (error) {
        if (error === 'wall') {
          wallAhead = true
          break
        }

        throw error
      }
    }
  }

  return new Promise(resolve => resolve(coords))
}
